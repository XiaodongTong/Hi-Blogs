﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogs.ModelDB.Entities
{
    /// <summary>
    /// 博客信息
    /// </summary>
    [Table("BlogInfo")]
    public class BlogInfo : BlogEntityBase
    { 
        /// <summary>
        /// 文章内容
        /// </summary>
        public string BlogContent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BlogRemarks { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string BlogTitle { get; set; }
        /// <summary>
        /// 文章url
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// 是否是转发文章
        /// </summary>
        public Nullable<bool> IsForwarding { get; set; }
        /// <summary>
        /// 博客创建时间
        /// </summary>
        public Nullable<System.DateTime> BlogCreateTime { get; set; }
        /// <summary>
        /// 博客修改时间
        /// </summary>
        public Nullable<System.DateTime> BlogUpTime { get; set; }
        ///// <summary>
        ///// 用户ID（外键）
        ///// </summary>
        //public int UsersId { get; set; }
        /// <summary>
        /// 文章阅读量
        /// </summary>
        public Nullable<int> BlogReadNum { get; set; }
        /// <summary>
        /// 转发原链接
        /// </summary>
        public string BlogForUrl { get; set; }
        /// <summary>
        /// 是否显示在首页
        /// </summary>
        public Nullable<bool> IsShowHome { get; set; }
        /// <summary>
        /// 是否显示在个人主页
        /// </summary>
        public Nullable<bool> IsShowMyHome { get; set; }
        /// <summary>
        /// 此博客文章的评论数
        /// </summary>
        public Nullable<int> BlogCommentNum { get; set; }

        /// <summary>
        /// 博客评论
        /// </summary>      
        public virtual ICollection<BlogComment> BlogComments { get; set; }

        /// <summary>
        /// 博客用户
        /// </summary>
        public virtual BlogUser BlogUser { get; set; }

        /// <summary>
        /// 博客标签
        /// </summary>       
        public virtual ICollection<BlogTag> BlogTags { get; set; }

        /// <summary>
        /// 博客标签
        /// </summary> 
        public virtual ICollection<BlogType> BlogTypes { get; set; }

        /// <summary>
        /// 博客阅读统计
        /// </summary>      
        public virtual ICollection<BlogReadInfo> BlogReadInfos { get; set; }
    }
}
